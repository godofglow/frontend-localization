# README #

### What is this repository for? ###

This is a repository to store localization files for the KUKA Connect frontend app.

Changes made to strings in this repository will be reflected in the product at https://www-dev.kuka-atx.com within 10 seconds of that change being committed to BitBucket. Pretty cool, no?

### Getting started

**Note:** These instructions will walk you through the process of making changes to our translation string tables such that you can view them in real-time on the actual web application.
If you do not require this level of interactivity, you can simply request us to send you the English string tables directly and then send back the results via email or whatever other digital transfer mechanism you prefer.

#### Get a KUKA Connect account
If one hasn't already been created for you, contact myself (james.juhasz@kukanao.com), Christy Gentile (christy.gentile@kukanao.com) or Emre Coklar (emre.coklar@kukanao.com) to get you setup.

Once you have an account, log in to https://www-dev.kuka-atx.com and make sure you can login and see the application.

#### Change your display language
Change the display language of the app to the language you're going to be translating into:

* Click on the main menu icon on the top left of the screen
* Select "User Settings"
* Under "Language Preferences" click the dropdown and select your language
* Click "Save All"
* Note that you may not see any change if no strings have yet been translated

#### Create a BitBucket account
https://bitbucket.org/account/signup/

> Once you have a BitBucket account, email the BitBucket username to james.juhasz@kukanao.com so I can give you write access to our repository. 
> You should be able to read the files regardless, but you won't be able to push your changes to BitBucket until I give you permission.

#### Learn Git (optional)
Start here:
https://www.atlassian.com/git/?utm_source=bitbucket&utm_medium=link&utm_campaign=help_dropdown&utm_content=learn_git

Note: you do not have to become a master of Git as it can be confusing. All the commands you need to do translation and see the results are included in this document.

#### Clone the repository
Install Git on your development machine if you haven't already:
https://www.atlassian.com/git/tutorials/install-git (instructions for Mac, Windows and Linux, just choose the base installer for your platform)

Open a terminal or command prompt, create a new directory for this project by issuing the following commands:

```
mkdir kuka-connect
cd kuka-connect
```

Then type:

```git clone https://<your-bitbucket-username>@bitbucket.org/kukaaustin/frontend-localization.git```

Finally:

```cd frontend-localization```

Now you're ready to make changes!

### HOWTO ###

#### The translation files
Located in the root directory of this repository are files with the following pattern:
```locale-XX.json```
Where XX is the [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) language code of the language you're going to translate into.

**Note: These files must be saved using UTF-8 encoding.**

Currently, there are three files you should be concerned with:

* locale-en.json (English)
* locale-de.json (German)
* locale-zh-Hans.json (Simplified Chinese)
 
The source of truth will always be ```locale-en.json```.

##### New languages
Adding new languages requires application level support. When a new language file is ready to be translated, its locale-XX.json file will appear here.
 
Inside these files is a single json object structured like a string table:

```
{
  "string.key1": "string.value1",
  "string.key2": "string.value2",
  ...
}
```

Open this file in the editing tool of choice and start making changes.

##### String Key Names
We have tried to name the string keys relative to where they appear in the User Interface.
For strings that are global to the app:

```strings.<area>.<id>```

Ex:

```"strings.dialogs.OK": "OK"```

For strings that appear in a particular view:

```views.<page>.<card>[.<subview>].id```

Ex:

```"views.faultLog.faultTable.FaultCodeTableHeader": "Alarm Id"```

Untranslated strings in non "en" files will naturally be in English. Most string values are fairly straightforward and should be easy to translate. **There are three exceptions, however:**

##### Strings with markup
Some strings will have HTML markup in them. Ignore any standard markup.

Ex:

```"strings.upgradeToPlus": "Upgrade to <b>PLUS</b>"```

Ignore the <b> and </b>, but translate what is inside it.

##### Formatted strings
String values of the form:

```"This is a string {parameter}"``` 
are strings with a variable parameter, any value inside the {} should **not** be translated nor changed, created or removed.

Ex:

```"An error occurred: {errorMessage}"``` in German (excuse my Google translate):

```"ein Fehler ist aufgetreten: {errorMessage}"```

In Chinese:

```"发生错误：{errorMessage}"```

##### Pluralization
String values of the form:

```{param, plural, one{# thing} other{# things}}``` 

are strings that use [MessageFormat's](https://messageformat.github.io/) pluralization facility. If you see a string in this format, the **only** portion that should be translated is the string **inside** the inner curly braces {}. The "#" placeholder is replaced by the number and should remain but be positioned appropriately to the language. (See examples below)
Ex:

```"{weeks, plural, one{# week} other{# weeks}}"``` translated to German would be:

```"{weeks, plural, one{# Woche} other{# Wochen}}"```

In Chinese:

```"{weeks, plural, one{#周} other{#周}}"```

This allows the front-end developers to ignore the vagueries of pluralization rules and helps us avoid logic like:

```
if (numWeeks === 0) {
    return "None";
} else if (numWeeks === 1) {
    return "1 week";
} else {
    return numWeeks + " weeks";
}
```

#### Submitting your changes

Once you have made some changes and would like to view the translated strings in the User Interface do the following:

```git add locale-XX.json``` (where XX is the [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) code for the language you are translating into)

```git commit```

This will bring up your default text editor. Enter a commit message indicating the changes you've made and save the file.
This will commit your changes to your local machine, but these changes will **not yet** be reflected in BitBucket. To do that simply type:

```git push```

Once you've successfully pushed to BitBucket, your string should appear in the appropriate place in the User Interface at: https://www-dev.kuka-atx.com within 10 seconds. A page refresh will be required.

### Getting your translations into the released product

Once translation is complete, the development team will simply take your translated json file from here and include it in the released product. No further work is necessary on your part.

### Contact Info ###

* @jamesjuhasz
* @jlong4096